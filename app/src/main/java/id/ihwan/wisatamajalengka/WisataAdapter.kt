package id.ihwan.wisatamajalengka

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_wisata.view.*


/**
 * Created by Ihwan ID on 19,October,2018.
 * Subscribe my Youtube Channel => https://www.youtube.com/channel/UCjntzibNSsjjIOh0HoP9vxw
 * mynameisihwan@gmail.com
 */
class WisataAdapter(val context: Context,
                    val items: ArrayList<Wisata>,
                    private val listener: (Wisata) -> Unit)
    : RecyclerView.Adapter<WisataAdapter.ViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context)
            .inflate(R.layout.item_wisata, p0, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItem(items[p1],listener)
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bindItem(items: Wisata, listener: (Wisata) -> Unit){

            val imgUrl = "http://ngetripmajalengka.ercode.id/img/lokasi/"
            itemView.tvNama.text = items.nama
            itemView.tvAlamat.text = "${items.desa}, kecamatan ${items.kecamatan} "
            Glide.with(itemView.context)
                .load(imgUrl+items.gambar)
                .into(itemView.ivGambar)

            itemView.cvContainer.setOnClickListener {
                listener(items)
            }
        }

    }
}