package id.ihwan.wisatamajalengka


/**
 * Created by Ihwan ID on 19,October,2018.
 * Subscribe my Youtube Channel => https://www.youtube.com/channel/UCjntzibNSsjjIOh0HoP9vxw
 * mynameisihwan@gmail.com
 */
data class Wisata(
    val id: String,
    val nama: String,
    val see: String,
    val gambar: String,
    val desa: String,
    val kecamatan: String,
    val latitude: String,
    val longitude: String,
    val jenis: String,
    val alamat: String,
    val fasilitas: String
)