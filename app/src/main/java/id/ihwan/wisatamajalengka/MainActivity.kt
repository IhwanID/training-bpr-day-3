package id.ihwan.wisatamajalengka

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import org.json.JSONObject
import java.util.ArrayList


class MainActivity : AppCompatActivity() {

    val wisataArrayList = ArrayList<Wisata>()
    val baseUrl = "http://ngetripmajalengka.ercode.id/api/getLokasi"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getData()
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    fun getData() {
        wisataArrayList.clear()
        AndroidNetworking.get(baseUrl)
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener{
                override fun onResponse(response: JSONObject) {
                    Log.d("RESPONSE -->", response.toString())

                    val data = response.getJSONArray("data")

                    for (i in 0 until data.length()){
                        val  dataObject = data.getJSONObject(i)

                        wisataArrayList.add(
                            Wisata(
                                dataObject.getString("id_lokasi"),
                                dataObject.getString("lokasi_nama"),
                                dataObject.getString("lokasi_see"),
                                dataObject.getString("lokasi_gambar"),
                                dataObject.getString("desaNama"),
                                dataObject.getString("kecamatanNama"),
                                dataObject.getString("lokasi_latitude"),
                                dataObject.getString("lokasi_longitude"),
                                dataObject.getString("jenisNama"),
                                dataObject.getString("lokasi_alamat"),
                                dataObject.getString("lokasi_fasilitas")
                            )
                        )
                    }

                    recyclerView.adapter = WisataAdapter(this@MainActivity, wisataArrayList){
                                startActivity<DetailActivity>(
                                    "id" to it.id,
                                    "nama" to it.nama,
                                    "see" to it.see,
                                    "gambar" to it.gambar,
                                    "desa" to it.desa,
                                    "kecamatan" to it.kecamatan,
                                    "latitude" to it.latitude,
                                    "longitude" to it.longitude,
                                    "jenis" to it.jenis,
                                    "alamat" to it.alamat,
                                    "fasilitas" to it.fasilitas
                                )
                            }
                }

                override fun onError(anError: ANError) {

                }
            })

    }


}
